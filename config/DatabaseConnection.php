<?php

class DatabaseConnection
{
    // DB params
    private $host = 'localhost';
    private $dbName = 'api_example';
    private $userName = 'root';
    private $password = '150115';
    private $connection;

    // DB connect
    public function connect()
    {
        $this->connection = null;

        try {
            $this->connection = new PDO('mysql:host='.$this->host. ';dbname=' . $this->dbName, $this->userName, $this->password);

            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Connection Error: ' . $e->getMessage();
        }

        return $this->connection;
    }
}