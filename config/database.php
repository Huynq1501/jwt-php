<?php
require 'DatabaseConnection.php';
$defaultPass = password_hash(12345678, PASSWORD_BCRYPT);
$statement = <<<EOS
    CREATE TABLE IF NOT EXISTS user_access_token (
        id INT NOT NULL AUTO_INCREMENT,
        user_id VARCHAR(100) NOT NULL,
        token VARCHAR(255) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT 0,
        expired_at TIMESTAMP DEFAULT 0,
        revoked BOOLEAN DEFAULT 0,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    CREATE TABLE IF NOT EXISTS users (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(100) NOT NULL,
        email VARCHAR(100) NOT NULL,
        phone VARCHAR(20) DEFAULT NULL,
        password VARCHAR(100) DEFAULT 12345678,
        status INT DEFAULT 1,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        updated_at TIMESTAMP DEFAULT 0,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;
EOS;

try {
    $connection = (new DatabaseConnection())->connect();
    $createTable = $connection->exec($statement);

    echo "Success!\n";
} catch (\PDOException $e) {
    exit($e->getMessage());
}