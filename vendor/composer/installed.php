<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'lcobucci/clock' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'reference' => 'fb533e093fd61321bfcbac08b131ce805fe183d3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lcobucci/clock',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'lcobucci/jwt' => array(
            'pretty_version' => '4.1.5',
            'version' => '4.1.5.0',
            'reference' => 'fe2d89f2eaa7087af4aa166c6f480ef04e000582',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lcobucci/jwt',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'stella-maris/clock' => array(
            'pretty_version' => '0.1.4',
            'version' => '0.1.4.0',
            'reference' => '8a0a967896df4c63417385dc69328a0aec84d9cf',
            'type' => 'library',
            'install_path' => __DIR__ . '/../stella-maris/clock',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
