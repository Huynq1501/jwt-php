<?php

class User
{
    // DB stuff
    private $connection;
    private $table = 'users';

    public $id;
    public $name;
    public $email;
    public $phone;
    public $password;

    public function __construct($db)
    {
        $this->connection = $db;
    }

    public function findUser()
    {
        $query = 'SELECT * FROM ' . $this->table . ' WHERE email= :email';
        $stmt = $this->connection->prepare($query);

        //clean data
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->password = htmlspecialchars(strip_tags($this->password));

        // Bind data
        $stmt->bindValue(':email', $this->email);

        if ($stmt->execute()) {
            if ($stmt->rowCount() !== 1) {
                return false;
            }

            $user = $stmt->fetch();
            $hashPassword = $user['password'];

            if (!password_verify($this->password, $hashPassword)) {
                return false;
            }

            return json_encode(array(
                'status' => 200,
                'data' => $user
            ));
        }

        print_r("Error: %s. \n", $stmt->error);

        return false;
    }

    public function show(){

    }

}