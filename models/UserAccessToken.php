<?php

class UserAccessToken
{
    // DB stuff
    private $connection;
    private $table = 'user_access_token';

    public $id;
    public $user_id;
    public $token;
    public $created_at;
    public $updated_at;

    public function __construct($db)
    {
        $this->connection = $db;
    }

    public function createAccessToken($userId, $token)
    {
        $query = "INSERT INTO " . $this->table . "(user_id, token) VALUES(".$userId.",'$token')";

        $stmt = $this->connection->prepare($query);

        if ($stmt->execute()) {
            return true;
        }

        print_r("Error: %s. \n", $stmt->error);

        return false;
    }

}