<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include '../../config/DatabaseConnection.php';
include '../../models/User.php';
$config = require '../../config/constants.php';

$database = new DatabaseConnection();
$db = $database->connect();
$user = new User($db);

$auth = $_SERVER['HTTP_AUTHORIZATION'];
$auth = explode(' ', $auth)[1];

$data = json_decode(file_get_contents("php://input"), false);

var_dump($data->id);
