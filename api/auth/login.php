<?php
require '../../vendor/autoload.php';

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include '../../config/DatabaseConnection.php';
include '../../models/User.php';
include '../../models/UserAccessToken.php';
$config = require '../../config/constants.php';

//Instantiate

$database = new DatabaseConnection();
$db = $database->connect();

// Instantiate user object
$user = new User($db);
$data = json_decode(file_get_contents("php://input"), false);

$user->email = $data->email;
$user->password = $data->password;
$result = $user->findUser();
if (!$result) {
    echo json_encode(array(
        'status' => $config['HTTP_CODE']['not_found'],
        'message' => "Username or password is incorrect, please try again"
    ));
} else {
    $authUser = json_decode($result, false);

    $configuration = Configuration::forSymmetricSigner(
        new Sha256(),
        InMemory::base64Encoded('mBC5v1sOKVvbdEitdSBenu59nfNfhwkedkJVNabosTw=')
    );

    $now = new DateTimeImmutable();
    $token = $configuration->builder()
        ->issuedBy($authUser->data->email)
        ->permittedFor('user login')
        ->issuedAt($now)
        ->canOnlyBeUsedAfter($now)
        ->expiresAt($now->modify('+1 hour'))
        ->withHeader('foo', 'bar')
        ->getToken($configuration->signer(), $configuration->signingKey());

    // create user access token
    $userAccessToken = new UserAccessToken($db);
    $userAccessToken->createAccessToken($authUser->data->id, $token->toString());

    echo json_encode(array(
        'access_token' => $token->toString(),
        'status' => $config['HTTP_CODE']['success'],
        'message' => "Login success"
    ));
}
